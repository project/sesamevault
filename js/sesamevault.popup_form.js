/**
 * Create Drupal behavior for SV
 */
Drupal.behaviors.sesameVaultUpload = function(context) {
  // Create a new global SesameVault session
  if (Drupal.settings.sesamevault.username && Drupal.settings.sesamevault.password) {
    Drupal.settings.sesamevault.sv = new SesameVault(Drupal.settings.sesamevault.username, Drupal.settings.sesamevault.password);
  }
  
  // Default Status state
  $('#sesamevault-popup-status-state').html('0%');
}

/**
 * Form Submit Function
 */
var sesameVaultSubmit = function() {
  // Get global sv object
  var sv = Drupal.settings.sesamevault.sv;
  
  // Lets get a new upload key
  sv.upload('key', function(r) {
    // Get upload key from argument
    var upload_key = r.upload_key;
    // Set global
    Drupal.settings.sesamevault.upload_key = upload_key;
    // Build URL
    var upload_url = sv.server('upload') + '/v2/upload/new?upload_key=' + upload_key;
    // Add signature to URL
    upload_url = sv.add_sig_to_url(upload_url);
    
    // Now set the form's url
    document.getElementById('UploadForm').action = upload_url;
    
    // Get Progress
    sesameVaultProgress(upload_key);
    
    // On complete
    document.getElementById('Uploader').onload = function() {
      // Send key to field in original window
      opener.sesameVaultPopulateTest(Drupal.settings.sesamevault.field, r.upload_key);
      // Close window
      setTimeout('javascript:window.close()', 1000);
    }
    
    // Start the upload!
    document.getElementById('UploadForm').submit();  
  });
}

/**
 * Get Progress
 */
var sesameVaultProgress = function() {
  // Check for Key
  if (!Drupal.settings.sesamevault.upload_key || Drupal.settings.sesamevault.upload_key == undefined) {
    return false;
  }
  // Get global sv object
  var sv = Drupal.settings.sesamevault.sv;
  
  // Get progress
  var params = { upload_key: Drupal.settings.sesamevault.upload_key };
  var progress = sv.upload('progress', params, function(results) {
    // Check progress
    if (results != undefined) {
      // Update progress bar    
      var percent = Math.round(100 * results.received / results.size);
      $('#sesamevault-popup-status').css('width', percent + '%');  
      $('#sesamevault-popup-status-state').html(percent + '%');
        
      // Check if done
      if (results.state == 'done') {
        $('#sesamevault-popup-status').css('width', '100%');  
        $('#sesamevault-popup-status-state').html(Drupal.settings.sesamevault.done);
        return;
      }
      else if (results.state == 'uploading') {
        setTimeout('sesameVaultProgress()', 1000);
      }
      else {
        // ERROR
        alert('SV Upload Error.');
      }
    }
    else {
      alert('no progress results');
    }
  });
}