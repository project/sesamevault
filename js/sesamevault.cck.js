/**
 * Drupal behavior for CCK Popups
 */
Drupal.behaviors.sesameVaultPopUp = function(context) {
  // Since we only want to show link if JS is enabled, we add it here
  // Loop through each one text field.  mark each as proccessed as we go
  // so Drupal can run the behavoirs again
  $('.sesamevault-id-field:not(.sesameVaultPopUp-processed)')
    .addClass('sesameVaultPopUp-processed')
    .each(function(i) {
      var thisField = $(this);
  
      // Remove description, as it is used elsewhere for JS version
      thisField.next('.description').hide();
      
      // Add the uploader html
      $('#' + Drupal.settings.sesamevault.cckPopupLink[this.id].preview_container_id).after(
        Drupal.settings.sesamevault.cckPopupLink[this.id].uploader_themed
      );
      
      // Remove remove button if there is no video
      if (!thisField.val()) {
        $('#' + Drupal.settings.sesamevault.cckPopupLink[this.id].remove_container_id).hide();
      }
      
      // Hide input text field.
      thisField.hide();
    });
    
    // Click event of each popup link
    $('.sesamevault-popup').click(function() {
      popWindow = window.open(
        this.href, 
        'sesameVaultUploaderPopup', 
        'width=600,height=500,scrollbars=yes,status=yes,resizable=yes,toolbar=no,menubar=no'
      );
      return false;
    });
  
  // Remove check button
  $('.sesamevault-remove-value').click(function(e) {
    var thisField = $(this);
    var dataField = thisField.attr('rel');
    // Check if checked
    if (thisField.attr('checked')) {
      // Get value of SV ID and store it in this object
      thisField.data('svID', $('#' + dataField).val());
      $('#' + dataField).val('');
    } else {
      // Get data stored in this object and put it back
      $('#' + dataField).val(thisField.data('svID'));
    }
  });
}

/**
 * Function to handle return from popup
 */
var sesameVaultPopulateTest = function(inputField, key) {
  if (key && inputField) {
    // Input ID
    $('#'+ inputField).val(key);
    // Trigger event for Ajax
    var url = Drupal.settings.basePath + 'sesamevault/video_data/' + key;
    // TODO: better icon or localize
    $('#' + Drupal.settings.sesamevault.cckPopupLink[inputField].preview_container_id).html('Loading...');
    $('#' + Drupal.settings.sesamevault.cckPopupLink[inputField].preview_container_id).load(url);
    // Show remove button
    $('#' + Drupal.settings.sesamevault.cckPopupLink[inputField].remove_container_id).show();
  }
}