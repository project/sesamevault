<?php

/**
 * @file
 * This file holds the functions for the administrative
 * settings of SesameVault
 */

/**
 * Drupal Admin Form
 *
 * This function creates a form for the admin settings
 * of the SesameVault module.
 *
 * @see 
 *   http://api.drupal.org/api/file/developer/topics/forms_api_reference.html/6
 *
 * @return
 *   returns form processed by admin_settings
 */
function sesamevault_admin_setting($form_state) {
  // API Descriptiona dn Instructions
  $api_description = '
    <p>'. t('SesameVault allows you to create an API Account instead of using your username and password.  It is much more secure, as it can be restricted to specific operations and domains.  The following are some basic instructions.') .'</p>
    <ul>
      <li>'. t('Go to <a href="!account_url">your SesameVault account page</a>', array('!account_url' => 'http://app.sesamevault.com/account/')) .'</li>
      <li>'. t('Generate a key, that is locked to this domain and allows for any API call.') .'</li>
      <li>'. t('Put the generated Username and Password below.') .'</li>
    </ul>
  ';
  $js_api_description = t('You can <a href="!download_url">download the JS API library here</a>.  Enter the path that is relative to your Drupal insall.  If you download the library and unzip it in the the SesameVault module directory, you should end up with something like: %suggested_path',
    array(
      '!download_url' => 'http://sesamevault.com/developer/js/',
      '%suggested_path' => drupal_get_path('module', 'sesamevault') .'/javascript/sesamevault_api.js',
    )
  );
  $php_api_description = t('You can <a href="!download_url">download the PHP API library here</a>.  Enter the path that is relative to your Drupal insall.  If you download the library and unzip it in the the SesameVault module directory, you should end up with something like: %suggested_path',
    array(
      '!download_url' => 'http://sesamevault.com/developer/php/',
      '%suggested_path' => drupal_get_path('module', 'sesamevault') .'/php/sesamevault_api.php',
    )
  );
  
  $form['sesamevault_api_files'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Files'),
    '#description' => t('This module uses both the Javascript and PHP API libraries from SesameVault.  Currently it uses v2.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['sesamevault_api_files']['sesamevault_api_js'] = array(
    '#type' => 'textfield',
    '#title' => t('JS API Library Path'),
    '#description' => $js_api_description,
    '#maxlength' => 256,
    '#default_value' => variable_get('sesamevault_api_js', ''),
    '#required' => TRUE,
  );
  $form['sesamevault_api_files']['sesamevault_api_php'] = array(
    '#type' => 'textfield',
    '#title' => t('PHP API Library Path'),
    '#description' => $php_api_description,
    '#maxlength' => 256,
    '#default_value' => variable_get('sesamevault_api_php', ''),
    '#required' => TRUE,
  );

  $form['sesamevault_access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access Login Data'),
    '#description' => $api_description,
    '#collapsible' => TRUE,
    '#default_value' => variable_get('sesamevault_username', ''),
  );
  $form['sesamevault_access']['sesamevault_username'] = array(
    '#type' => 'textfield',
    '#title' => t('SesameVault API Key Username'),
    '#description' => t('Put in your SesameVault user generated for this site (see above).'),
    '#default_value' => variable_get('sesamevault_username', ''),
    '#required' => TRUE,
  );
  $form['sesamevault_access']['sesamevault_password'] = array(
    '#type' => 'textfield',
    '#title' => t('SesameVault API Key Password'),
    '#description' => t('Put in your SesameVault apssword generated for this site (see above).'),
    '#default_value' => variable_get('sesamevault_password', ''),
    '#required' => TRUE,
  );
  
  // The following should not be necessary, but is (see comments in initialize function)
  $form['sesamevault_login_access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login Data'),
    '#description' => t('This is the regular username and password.  This is required because of some bugs in the API.  This will hopefully be removed.  This data will only be used in a secure fashion on the site.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['sesamevault_login_access']['sesamevault_login_username'] = array(
    '#type' => 'textfield',
    '#title' => t('SesameVault Username'),
    '#description' => t('Put in your regular SesameVault Username.'),
    '#default_value' => variable_get('sesamevault_login_username', ''),
    '#required' => TRUE,
  );
  $form['sesamevault_login_access']['sesamevault_login_password'] = array(
    '#type' => 'textfield',
    '#title' => t('SesameVault Password'),
    '#description' => t('Put in your regular SesameVault Password.'),
    '#default_value' => variable_get('sesamevault_login_password', ''),
    '#required' => TRUE,
  );
  
  return system_settings_form($form);
}

/**
 * Form validation for admin settings
 */
function sesamevault_admin_setting_validate(&$form, $form_state) {
  // Check API files to exist
  $check_location = file_check_location($form_state['values']['sesamevault_api_js']);
  if (!file_exists($check_location)) {
    form_set_error('sesamevault_api_js', t('JS API Library not a valid file'));
  }
  $check_location = file_check_location($form_state['values']['sesamevault_api_php']);
  if (!file_exists($check_location)) {
    form_set_error('sesamevault_api_php', t('PHP API Library not a valid file'));
  }
}